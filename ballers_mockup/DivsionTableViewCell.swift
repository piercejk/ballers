//
//  DivsionTableViewCell.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/22/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class DivsionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //you're going to need the standings to be [Dictionary<String, String>]
    func addStandings(standings : [Dictionary<String, AnyObject>], division : String){
        
        let header = StandingsHeaderView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 30.5))
        header.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(header)
        header.setupView(division: division)
        let headerLeft = NSLayoutConstraint(item: header, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        
        let headerTop = NSLayoutConstraint(item: header, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        
        header.heightAnchor.constraint(equalToConstant: 30.5).isActive = true
        
        NSLayoutConstraint.activate([headerLeft, headerTop])
        
        var previousView = header as UIView
        
        for eachTeam in standings{ // this is how the new data is loaded into the 
            
            var newView = StandingsRowView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 30.5))
            
            newView.translatesAutoresizingMaskIntoConstraints = false
            
            self.addSubview(newView)
            newView.topAnchor.constraint(equalTo: previousView.bottomAnchor).isActive = true
            newView.setupView(standings: eachTeam)
            
            previousView = newView
        }
        
    }


}
