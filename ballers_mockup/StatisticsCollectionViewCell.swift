//
//  StatisticsCollectionViewCell.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/18/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class StatisticsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var statLabel: UILabel!
    
    @IBOutlet weak var firstImage: UIImageView!
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var firstStat: UILabel!
    @IBOutlet weak var secondImage: UIImageView!
    
    @IBOutlet weak var secondName: UILabel!
    
    @IBOutlet weak var secondStat: UILabel!
    
    @IBOutlet weak var thirdImage: UIImageView!
    
    @IBOutlet weak var thirdName: UILabel!
    
    @IBOutlet weak var thirdStat: UILabel!
    
    @IBOutlet weak var fourthImage: UIImageView!
    
    @IBOutlet weak var fourthName: UILabel!
    
    @IBOutlet weak var fourthStat: UILabel!
    
    @IBOutlet weak var fifthImage: UIImageView!
    
    @IBOutlet weak var fifthName: UILabel!
    
    @IBOutlet weak var fifthStat: UILabel!
    
}
