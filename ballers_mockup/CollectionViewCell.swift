//
//  CollectionViewCell.swift
//  ballers_mockup
//
//  Created by Wharton, Maxwell E on 4/22/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
}
