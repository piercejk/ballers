//
//  scoresTableCell.swift
//  ballers_mockup
//
//  Created by Wharton, Maxwell E on 4/17/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class scoresTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        firstPitchLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        firstPitchLabel.centerXAnchor.constraint(equalTo: awayHits.superview!.superview!.centerXAnchor).isActive = true
        firstPitchLabel.centerYAnchor.constraint(equalTo: awayLogo.centerYAnchor).isActive = true
        
        timeLabel.centerXAnchor.constraint(equalTo: homeHits.superview!.superview!.centerXAnchor).isActive = true
        timeLabel.centerYAnchor.constraint(equalTo: homeLogo.centerYAnchor).isActive = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var errorsLabel: UILabel!
    @IBOutlet weak var hitsLabel: UILabel!
    @IBOutlet weak var runsLabel: UILabel!
    @IBOutlet weak var inning: UILabel!
    @IBOutlet weak var awayLogo: UIImageView!
    @IBOutlet weak var awayName: UILabel!
    @IBOutlet weak var awayRuns: UILabel!
    @IBOutlet weak var awayHits: UILabel!
    @IBOutlet weak var awayErrors: UILabel!
    @IBOutlet weak var homeLogo: UIImageView!
    @IBOutlet weak var homeName: UILabel!
    @IBOutlet weak var homeRuns: UILabel!
    @IBOutlet weak var homeHits: UILabel!
    @IBOutlet weak var homeErrors: UILabel!
    @IBOutlet weak var firstPitchLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
}
