//
//  StandingsHeaderView.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/23/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class StandingsHeaderView: UIView {

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(division : String){
        
        
        self.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
     
        self.heightAnchor.constraint(equalToConstant: 30.5).isActive = true
        self.leftAnchor.constraint(equalTo: self.superview!.leftAnchor)
        
        
        let divisionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width/3-40, height: 20.5))
        
        divisionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        divisionLabel.font = divisionLabel.font.withSize(17)
        
        divisionLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.addSubview(divisionLabel)
        
        let divisionConstraintLeft = NSLayoutConstraint(item: divisionLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 20)
        
        let divisionHeightContrstaint = NSLayoutConstraint(item: divisionLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([divisionConstraintLeft, divisionHeightContrstaint])
        
        
        
        let wLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        
        wLabel.font = wLabel.font.withSize(17)
        let lLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        lLabel.font = lLabel.font.withSize(17)
        let pctLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        pctLabel.font = pctLabel.font.withSize(17)
        let gbLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        gbLabel.font = gbLabel.font.withSize(17)
        self.addSubview(wLabel)
        self.addSubview(lLabel)
        self.addSubview(pctLabel)
        self.addSubview(gbLabel)
        
        wLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        pctLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        gbLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        
        
        wLabel.translatesAutoresizingMaskIntoConstraints = false
        lLabel.translatesAutoresizingMaskIntoConstraints = false
        pctLabel.translatesAutoresizingMaskIntoConstraints = false
        gbLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        
        divisionLabel.text = division
        wLabel.text = "W"
        lLabel.text = "L"
        pctLabel.text = "W%"
        gbLabel.text = "GB"
        
        /*
        
        wLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        lLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        pctLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
 */
        gbLabel.widthAnchor.constraint(equalToConstant: 35).isActive = true
 
      
        
        let sepConstant = (2/15) * self.frame.width - (0.5)*wLabel.frame.width
        
        let midSep = (2/15) * self.frame.width
        
        
        let gbRight = NSLayoutConstraint(item: gbLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -sepConstant)
        
        let pctRight = NSLayoutConstraint(item: pctLabel, attribute: .right, relatedBy: .equal, toItem: gbLabel, attribute: .left, multiplier: 1, constant: -midSep)
        
        let lRight = NSLayoutConstraint(item: lLabel, attribute: .right, relatedBy: .equal, toItem: pctLabel, attribute: .left, multiplier: 1, constant: -midSep-20)
        
        let wRight = NSLayoutConstraint(item: wLabel, attribute: .right, relatedBy: .equal, toItem: lLabel, attribute: .left, multiplier: 1, constant: -midSep-8)
        
        let gbY = NSLayoutConstraint(item: gbLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let pctY = NSLayoutConstraint(item: pctLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let lY = NSLayoutConstraint(item: lLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let wY = NSLayoutConstraint(item: wLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([gbRight, gbY])
        NSLayoutConstraint.activate([pctRight, pctY])
        NSLayoutConstraint.activate([lRight, lY])
        NSLayoutConstraint.activate([wRight, wY])
    
    }
    
}
