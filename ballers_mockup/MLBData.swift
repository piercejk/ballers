//
//  MLBData.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 5/1/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import Foundation

class MLBData{
    private let urlSession = URLSession.shared
    private var urlPathBase = "https://backballers.appspot.com/ballers/api/"
    private var dataTask:URLSessionDataTask? = nil
    
    var delegate:MLBDataProtocol? = nil
    
    init(){}
    
    func getTeamData(team : String) { // you could potentially handle the url request in this data task, and then call to a method with it.
        let urlPath = self.urlPathBase + "team/" + team
        let url:NSURL? = NSURL(string: urlPath)
        
        let dataTask = self.urlSession.dataTask(with: url! as URL) { (data, response, error) -> Void in
            if error != nil {
                print(error!) //this means that you've come accross some kind of error and I still think we would hande it in the way below
            }else{
                do{
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>
                    let data = jsonResult!
                    
                    if data.keys.count == 0{
                        self.delegate?.responseError()
                    }else{
                        self.delegate?.responseDataHandler(data: jsonResult!)
                    }
                }catch{
                    self.delegate?.responseError()}
            }
        }
        
        dataTask.resume()
    }
    
    
    
    func getScoresData(date : String) { // you could potentially handle the url request in this data task, and then call to a method with it.
        let urlPath = "https://backballers.appspot.com/ballers/api/scores/" + date
        let url:NSURL? = NSURL(string: urlPath)
        
        let dataTask = self.urlSession.dataTask(with: url! as URL) { (data, response, error) -> Void in
            if error != nil {
                print(error!) //this means that you've come accross some kind of error and I still think we would hande it in the way below
            }else{
                do{
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>
                    let data = jsonResult! as! Dictionary<String, Any>
                    
                    if data.count == 0{
                        self.delegate?.responseError()
                    }else{
                        self.delegate?.responseDataHandler(data: jsonResult!)
                    }
                }catch{self.delegate?.responseError()}
            }
        }
        
        dataTask.resume()
    }
    
    func getStandingsData() { // you could potentially handle the url request in this data task, and then call to a method with it.
        let base = "standings"
        let urlPath = self.urlPathBase + base
        let url:NSURL? = NSURL(string: urlPath)
        
        let dataTask = self.urlSession.dataTask(with: url! as URL) { (data, response, error) -> Void in
            if error != nil {
                print(error!) //this means that you've come accross some kind of error and I still think we would hande it in the way below
            }else{
                do{
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>
                    let data = jsonResult! as! Dictionary<String, Any>
                    
                    if data.keys.count == 0{
                        self.delegate?.responseError()
                    }else{
                        self.delegate?.responseDataHandler(data: jsonResult!)
                    }
                }catch{self.delegate?.responseError()}
            }
        }
        
        dataTask.resume()
    }
    
    func getStatsData() { // you could potentially handle the url request in this data task, and then call to a method with it.
        let base = "stats"
        let urlPath = self.urlPathBase + base
        let url:NSURL? = NSURL(string: urlPath)
        
        let dataTask = self.urlSession.dataTask(with: url! as URL) { (data, response, error) -> Void in
            if error != nil {
                print(error!) //this means that you've come accross some kind of error and I still think we would hande it in the way below
            }else{
                do{
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>
                    let data = jsonResult! as! Dictionary<String, Any>
                    
                    if data.count == 0{
                        self.delegate?.responseError()
                    }else{
                        self.delegate?.responseDataHandler(data: jsonResult!)
                    }
                }catch{self.delegate?.responseError()}
            }
        }
        
        dataTask.resume()
    }
    
    
}

protocol MLBDataProtocol {
    func responseDataHandler(data :Dictionary<String, Any>)
    func responseError()
    
}
