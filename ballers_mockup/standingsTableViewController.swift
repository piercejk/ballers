//
//  standingsTableViewController.swift
//  ballers_mockup
//
//  Created by Wharton, Maxwell E on 4/17/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class standingsTableViewController: UITableViewController, MLBDataProtocol{

    
    let baseball = Baseball()

    var dataReady = false
    var dataSession = MLBData()

    var data : Dictionary<String,Any>? = nil
    
    func addBaseball(){
        self.view.addSubview(self.baseball)
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        tableView.tableFooterView = UIView()
        baseball.translatesAutoresizingMaskIntoConstraints = false
        baseball.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        baseball.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        baseball.widthAnchor.constraint(equalToConstant: 100).isActive = true
        baseball.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.rotateView(targetView: self.baseball)
    }
    
    private func rotateView(targetView: UIView, duration: Double = 2.5) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(M_PI))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
    
    override func viewDidLoad() {
        addBaseball()
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.tableView.tableFooterView = UIView()
        navigationController?.navigationBar.topItem?.title = "Standings"
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        super.viewDidLoad()
        self.dataSession.delegate = self
        self.dataSession.getStandingsData()
        tableView.register(DivsionTableViewCell.self, forCellReuseIdentifier: "division")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "header")
        
    }
    
    
    func getNumCells(section : Int) -> Int{
        if section <= 1{return 4}
        else{return 3}
    }
    
    func getLeague(indexPath : IndexPath) -> String{
        if indexPath.section == 0{return "American League"}
        else if indexPath.section == 1{ return "National League"}
        else{return "WC"}
    }

    func responseError() {
        print("an error has occured")
    }
    
    
    func responseDataHandler(data: Dictionary<String, Any>) {
        self.data = data
        
        self.dataReady = true
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 50
        }
        
        if indexPath.section <= 1{
            return 200
        }else{
            return 400
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection: Int) -> Int {
        
        if !self.dataReady{
            baseball.isHidden = false
            return 0
        }
        baseball.isHidden = true
        return self.getNumCells(section: numberOfRowsInSection)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
            
            var league = self.getLeague(indexPath: indexPath)
            if league == "WC"{
                league = "Wild Card"
            }
            
            label.text = self.getLeague(indexPath: indexPath)
            label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            label.font = UIFont(name: "Verdana-Bold", size: 18)
            
            cell.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            
            label.centerYAnchor.constraint(equalTo: cell.centerYAnchor).isActive = true
            
            label.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
            
            label.widthAnchor.constraint(equalToConstant: 200).isActive = true
            label.heightAnchor.constraint(equalToConstant: cell.frame.height).isActive = true
            
            cell.backgroundColor = UIColor.black
            
            label.textAlignment = NSTextAlignment.center
           
            return cell
            
        }
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "division") as! DivsionTableViewCell
        let cell = DivsionTableViewCell(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 100))
        
        
        cell.layer.borderWidth = 1
        cell.layer.borderColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
        cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        var confData = self.data![self.getLeague(indexPath: indexPath)] as! Dictionary<String, [Any]>
        
        var data = Array(confData.values)[indexPath.row-1] as! [Dictionary<String, AnyObject>]
        
        var division = Array(confData.keys)[indexPath.row-1] as! String
        
  
        if self.getLeague(indexPath: indexPath) == "WC"{
            for allVals in 0..<data.count{
                data[allVals]["gamesBack"] = data[allVals]["wcGamesBack"]
            }
            
            if division == "American League"{
                division = "AL"
            }else{ division = "NL"}
            
        }
        
        cell.addStandings(standings: data, division: division)
        
        return cell
    }

}
