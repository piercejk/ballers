import UIKit
import CoreData

class homeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, MLBDataProtocol {
    
    var person: [NSManagedObject] = []
    

    func responseError() { //we got a response error when getting the shit we needed.
        print("We have a problem.")
    }
    
    let imageArray = [UIImage(named: "ARI"), UIImage(named: "ATL"), UIImage(named: "BAL"), UIImage(named: "BOS"), UIImage(named: "CHC"), UIImage(named: "CIN"), UIImage(named: "CLE"), UIImage(named: "COL"), UIImage(named: "CWS"), UIImage(named: "DET"), UIImage(named: "HOU"), UIImage(named: "KC"), UIImage(named: "LAA"), UIImage(named: "LAD"), UIImage(named: "MIA"), UIImage(named: "MIL"), UIImage(named: "MIN"), UIImage(named: "NYM"), UIImage(named: "NYY"), UIImage(named: "OAK"), UIImage(named: "PIT"), UIImage(named: "PHI"), UIImage(named: "SD"), UIImage(named: "SEA"), UIImage(named: "SF"), UIImage(named: "STL"), UIImage(named: "TB"), UIImage(named: "TEX"), UIImage(named: "TOR"), UIImage(named: "WAS")]
    
    let teamArray = ["ARI", "ATL", "BAL", "BOS", "CHC", "CIN", "CLE", "COL", "CWS", "DET", "HOU", "KC", "LAA", "LAD", "MIA", "MIL", "MIN", "NYM", "NYY", "OAK", "PIT", "PHI", "SD", "SEA", "SF", "STL", "TB", "TEX", "TOR", "WAS"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! CollectionViewCell
        
        cell.img.image = imageArray[indexPath.row]
        cell.layer.cornerRadius = 25
        cell.layer.masksToBounds = true
        
        
        collectionView.backgroundColor = UIColor.darkGray
        
        return cell
    }
    
    var selectedIndexPath: IndexPath?
    
    var teamAbbreviation = "ARI" //default team selection
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        teamAbbreviation = teamArray[indexPath.row]
        let cell = collectionView.cellForItem(at:indexPath)
        cell?.layer.borderColor = UIColor.red.cgColor
        cell?.layer.borderWidth = 5
        self.selectedIndexPath = indexPath
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at:indexPath)
        cell?.layer.borderWidth = 0
        self.selectedIndexPath = nil
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    var dataSession = MLBData()
    
    @IBAction func saveTeam(_ sender: Any) {
        
        //This code is supposed to save the data once they select a team
        //Then it calls the data session to make the data show up
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "User",
                                       in: managedContext)!
        
        let user = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        user.setValue(teamAbbreviation, forKeyPath: "favoriteTeam")
        
        do {
            try managedContext.save()
            person.append(user)
        } catch _ as NSError {
            print("Could not save.")
        }
        
        self.dataSession.getTeamData(team: teamAbbreviation)
        
    }
    
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var standingLabel: UILabel!
    @IBOutlet weak var baLabel: UILabel!
    @IBOutlet weak var hrLabel: UILabel!
    @IBOutlet weak var diffLabel: UILabel!
    @IBOutlet weak var eraLabel: UILabel!
    @IBOutlet weak var soLabel: UILabel!
    @IBOutlet weak var whipLabel: UILabel!
    @IBOutlet weak var selectTeam: UILabel!
    
    func responseDataHandler(data: Dictionary<String, Any>) {
        
        //getting dictionaries
        let stats = data["stats"] as! Dictionary<String, Any>
        let battingStats = stats["batting"] as! Dictionary<String, Any>
        let pitchingStats = stats["pitching"] as! Dictionary<String, Any>
        let standings = stats["standings"] as! Dictionary<String, Any>
        let teamInfo = data["team"] as! Dictionary<String, Any>
        
        //getting data
        let teamCity = teamInfo["city"] as! String
        let teamMascot = teamInfo["name"] as! String
        teamAbbreviation = teamInfo["abbreviation"] as! String
        let teamWins = standings["wins"]!
        let teamLosses = standings["losses"]!
        let teamGB = standings["gamesBack"]!
        let teamBA = battingStats["battingAvg"]!
        let teamHR = battingStats["homeruns"]!
        let teamDiff = standings["runDifferential"]!
        let teamERA = pitchingStats["earnedRunAvg"]!
        let teamSO = pitchingStats["pitcherStrikeouts"]!
        let teamWHIP = pitchingStats["walksAndHitsPerInningPitched"]!
        
        DispatchQueue.main.async(){
            
            //displaying data
            self.teamLabel.text = teamCity + " " + teamMascot
            self.teamImage.image = UIImage(named: self.teamAbbreviation)
            self.recordLabel.text = "Record: \(teamWins)-\(teamLosses)"
            self.standingLabel.text = "Standing: \(teamGB) games back"
            self.baLabel.text = "BA: \(teamBA)"
            self.hrLabel.text = "HR: \(teamHR)"
            self.diffLabel.text = "DIFF: \(teamDiff)"
            self.eraLabel.text = "ERA: \(teamERA)"
            self.soLabel.text = "SO: \(teamSO)"
            self.whipLabel.text = "WHIP: \(teamWHIP)"
            self.selectTeam.text = nil
            
        }
        
        
    }
    
    @IBOutlet weak var teamInfoView: UIView!
    @IBOutlet weak var setTeamButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTeamButton.layer.cornerRadius = 10
        self.setTeamButton.layer.masksToBounds = true
        self.teamInfoView.layer.cornerRadius = 20
        self.teamInfoView.layer.masksToBounds = true
        self.teamInfoView.backgroundColor = UIColor.darkGray
        
        navigationController?.navigationBar.topItem?.title = "Home"
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        
        self.view.backgroundColor = UIColor.black
        
        
        
        
        self.dataSession.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Below is supposed to fetch core data and make their favorite team show up
        //If they dont have their favorite team selected, the "Please Select a Team!" text shows up
        
        super.viewWillAppear(animated)
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
    
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "User")
        
        do {
            person = try managedContext.fetch(fetchRequest)
        } catch _ as NSError {
            print("Could not fetch.")
        }
        
        if person.count != 0{
            
            let user = person[0]
            user.value(forKeyPath: "favoriteTeam")
            teamAbbreviation = user.value(forKeyPath: "favoriteTeam") as! String
            saveTeam((Any).self)
        } else{
            self.teamLabel.text = nil
            self.teamImage.image = nil
            self.recordLabel.text = nil
            self.standingLabel.text = nil
            self.baLabel.text = nil
            self.hrLabel.text = nil
            self.diffLabel.text = nil
            self.eraLabel.text = nil
            self.soLabel.text = nil
            self.whipLabel.text = nil
            self.selectTeam.text = "Please Select a Team!"
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
}
