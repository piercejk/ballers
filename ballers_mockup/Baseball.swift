//
//  Baseball.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 5/10/19.
//  Copyright © 2019 ballers. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable class Baseball : UIView{
    
    private struct Constants{
        static let lineWidth: CGFloat = 8.0
        static let arcWidth : CGFloat = 8.0
    }
    
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        
        let center = CGPoint(x:bounds.width / 2, y : bounds.width/2)
        let radius : CGFloat = max(bounds.width, bounds.height)
        
        let path = UIBezierPath(arcCenter: center, radius: radius/2-Constants.arcWidth/2, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        
        path.lineWidth = Constants.arcWidth
        context?.setStrokeColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        path.stroke()
        
        
        let seamR = UIBezierPath(arcCenter: CGPoint(x: center.x + radius * 0.645, y: center.y), radius: radius/2, startAngle: 0.75 * .pi, endAngle: 1.25 * .pi, clockwise: true)
        
        seamR.lineWidth = Constants.arcWidth
        context?.setStrokeColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        seamR.stroke()
        
        
        let seamL = UIBezierPath(arcCenter: CGPoint(x: center.x - radius * 0.645, y: center.y), radius: radius/2, startAngle: 0.25 * .pi, endAngle: 1.75 * .pi, clockwise: false)
        
        seamL.lineWidth = Constants.arcWidth
        
        seamL.stroke()
    }
    
    
}
