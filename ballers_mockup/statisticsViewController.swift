//
//  statisticsViewController.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/18/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class statisticsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, MLBDataProtocol {
    
    
    let baseball = Baseball()
    var dataSession = MLBData()
    var stats : Dictionary<String, Any> = [:]
    var dataHere = false
    var image : UIImage? = nil
    var dataReady = false
    var imageReady = false
    
    
    func responseDataHandler(data:Dictionary<String, Any>) {
        
        stats = data
        self.dataReady = true
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
    
    
    func addBaseball(){
        self.view.addSubview(self.baseball)
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        baseball.translatesAutoresizingMaskIntoConstraints = false
        baseball.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        baseball.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        baseball.widthAnchor.constraint(equalToConstant: 100).isActive = true
        baseball.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.rotateView(targetView: self.baseball)
    }
    
    private func rotateView(targetView: UIView, duration: Double = 2.5) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(M_PI))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
    
    func responseError() {
        DispatchQueue.main.async {
            print("an error has occured")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBaseball()
        view.backgroundColor = UIColor.black
        collectionView?.backgroundColor = UIColor.black
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        navigationController?.navigationBar.topItem?.title = "Stats"
        self.dataSession.delegate = (self as MLBDataProtocol)
        self.dataSession.getStatsData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension statisticsViewController{
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !self.dataReady{
            self.baseball.isHidden = false
            return 0
        }
        self.baseball.isHidden = true
        return 18
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "id", for: indexPath) as! StatisticsCollectionViewCell
        
           // cell.layer.borderColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
            //cell.layer.borderWidth = 3
            var nameCat = ""
            var ALData = stats["AL"] as! Dictionary<String, Any>
            let loc = indexPath.row % 6
            if indexPath.row < 6{
                ALData = stats["AL"] as! Dictionary<String, Any>
                nameCat = "AL"
            }
            else if (indexPath.row >= 6) && (indexPath.row < 12){
                ALData = stats["NL"] as! Dictionary<String, Any>
                nameCat = "NL"
            }
            else {
                ALData = stats["MLB"] as! Dictionary<String, Any>
                nameCat = "MLB"
            }
            var category = ALData["homeruns"] as! NSArray
            var key = ""
            switch loc
            {
            case 0:
                key = "homeruns"
                category = ALData["homeruns"] as! NSArray
                nameCat += " Home Run"
            case 1:
                category = ALData["pitcherStrikeouts"] as! NSArray
                key = "pitcherStrikeouts"
                nameCat += " Home Run"
            case 2:
                category = ALData["battingAvg"] as! NSArray
                key = "battingAvg"
                nameCat += " Batting Average"
            case 3:
                category = ALData["wins"] as! NSArray
                key = "wins"
                nameCat += " Wins"
            case 4:
                category = ALData["earnedRunAvg"] as! NSArray
                key = "earnedRunAvg"
                nameCat += " Earned Run Average"
            case 5:
                category = ALData["runsBattedIn"] as! NSArray
                key = "runsBattedIn"
                nameCat += " Runs Batted In"
            default:
                break
            }
            var icon = ""
            var photo = ""
            let url = "https://securea.mlb.com/mlb/images/players/head_shot/"
            let pl1 = category[0] as! Dictionary<String, Any>
            let playerName1 = "\(pl1["firstName"] as! String) \(pl1["lastName"] as! String)"
            let score1 = pl1[key] as! NSNumber
            icon = pl1["id"] as! String
            photo =  url + icon + ".jpg"
        
            
            cell.firstImage.image = UIImage(named: "icon1")
            cell.firstImage.downloadImageFrom(link: photo, contentMode: UIViewContentMode.scaleAspectFit)
            
            
            let pl2 = category[1] as! Dictionary<String, Any>
            let playerName2 = "\(pl2["firstName"] as! String) \(pl2["lastName"] as! String)"
            let score2 = pl2[key] as! NSNumber
            icon = pl2["id"] as! String
            photo =  url + icon + ".jpg"
        
            
            cell.secondImage.image = UIImage(named: "icon1")
            cell.secondImage.downloadImageFrom(link: photo, contentMode: UIViewContentMode.scaleAspectFit)
            
            let pl3 = category[2] as! Dictionary<String, Any>
            let playerName3 = "\(pl3["firstName"] as! String) \(pl3["lastName"] as! String)"
            let score3 = pl3[key] as! NSNumber
            icon = pl3["id"] as! String
            photo =  url + icon + ".jpg"
        
            
            cell.thirdImage.image = UIImage(named: "icon1")
            cell.thirdImage.downloadImageFrom(link: photo, contentMode: UIViewContentMode.scaleAspectFit)
            
            
            let pl4 = category[3] as! Dictionary<String, Any>
            let playerName4 = "\(pl4["firstName"] as! String) \(pl4["lastName"] as! String)"
            let score4 = pl4[key] as! NSNumber
            icon = pl4["id"] as! String
            photo =  url + icon + ".jpg"
        
            
            cell.fourthImage.image = UIImage(named: "icon1")
            cell.fourthImage.downloadImageFrom(link: photo, contentMode: UIViewContentMode.scaleAspectFit)
        
            let pl5 = category[4] as! Dictionary<String, Any>
            let playerName5 = "\(pl5["firstName"] as! String) \(pl5["lastName"] as! String)"
            let score5 = pl5[key] as! NSNumber
            icon = pl5["id"] as! String
            photo =  url + icon + ".jpg"
        
            
            cell.fifthImage.image = UIImage(named: "icon1")
            cell.fifthImage.downloadImageFrom(link: photo, contentMode: UIViewContentMode.scaleAspectFit)
            
            
            cell.firstName.text = playerName1
            cell.secondName.text = playerName2
            cell.thirdName.text = playerName3
            cell.fourthName.text = playerName4
            cell.fifthName.text = playerName5
            cell.statLabel.text = nameCat
            
            cell.firstStat.text = "\(score1)"
            cell.secondStat.text = "\(score2)"
            cell.thirdStat.text = "\(score3)"
            cell.fourthStat.text = "\(score4)"
            cell.fifthStat.text = "\(score5)"
        
        circImage(image: cell.firstImage)
        circImage(image: cell.secondImage)
        circImage(image: cell.thirdImage)
        circImage(image: cell.fourthImage)
        circImage(image: cell.fifthImage)
        
        
        return cell
    }
    
    func circImage(image : UIImageView){
        
        image.layer.cornerRadius = image.frame.height/2
        image.layer.borderWidth = 1
        image.layer.masksToBounds = false
        image.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        image.clipsToBounds = true
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 200, height: 250)
    }
    
    
}


extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

