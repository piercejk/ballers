//
//  StandingsRowView.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/23/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class StandingsRowView: UIView {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(standings : Dictionary<String, AnyObject>){
        
        self.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        
        
        
        self.heightAnchor.constraint(equalToConstant: 30.5).isActive = true
        self.leftAnchor.constraint(equalTo: self.superview!.leftAnchor).isActive = true
        
        let teamImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        teamImage.layer.cornerRadius = 0.5 * teamImage.bounds.size.width
        teamImage.clipsToBounds = true
        
        teamImage.translatesAutoresizingMaskIntoConstraints = false
        
        
        /*
        let teamImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 33, height: 19))
        
        
        teamImage.translatesAutoresizingMaskIntoConstraints = false
        */
        teamImage.heightAnchor.constraint(equalToConstant: 25).isActive = true
        teamImage.widthAnchor.constraint(equalToConstant: 25).isActive = true
 
        let teamLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 20.5))
        teamLabel.translatesAutoresizingMaskIntoConstraints = false
        teamLabel.font = teamLabel.font.withSize(17)
        teamLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.addSubview(teamImage)
        self.addSubview(teamLabel)
        
       let sc_width = UIScreen.main.bounds.width
        
        let teamImageLeft = NSLayoutConstraint(item: teamImage, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 30)
        
        let teamLabelRight = NSLayoutConstraint(item: teamLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -(2/3) * sc_width - 20)
        
        let teamImageCenter = NSLayoutConstraint(item: teamImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let teamLabelCenter = NSLayoutConstraint(item: teamLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        
        
        NSLayoutConstraint.activate([teamImageLeft, teamLabelRight, teamImageCenter, teamLabelCenter])
        
        let teamwLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        
        teamwLabel.translatesAutoresizingMaskIntoConstraints = false
        
        teamwLabel.font = teamwLabel.font.withSize(17)
        let teamlLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        teamlLabel.translatesAutoresizingMaskIntoConstraints = false
        teamlLabel.font = teamlLabel.font.withSize(17)
        let teampctLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        teampctLabel.translatesAutoresizingMaskIntoConstraints = false
        teampctLabel.font = teampctLabel.font.withSize(17)
        let teamgbLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 31.5, height: 20.5))
        teamgbLabel.translatesAutoresizingMaskIntoConstraints = false
        teamgbLabel.font = teamgbLabel.font.withSize(17)
        
        teamlLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        teampctLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        teamwLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        teamgbLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        teamgbLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.addSubview(teamwLabel)
        self.addSubview(teamlLabel)
        self.addSubview(teampctLabel)
        self.addSubview(teamgbLabel)
        
        teamgbLabel.widthAnchor.constraint(equalToConstant: 35).isActive = true
 
        
        let sepConstant = (2/15) * self.frame.width - (0.5) * teamwLabel.frame.width
        let midSep = (2/15) * self.frame.width
        
        
        let teamgbRight = NSLayoutConstraint(item: teamgbLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -sepConstant)
        
        let teampctRight = NSLayoutConstraint(item: teampctLabel, attribute: .right, relatedBy: .equal, toItem: teamgbLabel, attribute: .left, multiplier: 1, constant: -midSep)
        
        let teamlRight = NSLayoutConstraint(item: teamlLabel, attribute: .right, relatedBy: .equal, toItem: teampctLabel, attribute: .left, multiplier: 1, constant: -midSep)
        
        let teamwRight = NSLayoutConstraint(item: teamwLabel, attribute: .right, relatedBy: .equal, toItem: teamlLabel, attribute: .left, multiplier: 1, constant: -midSep)
        
        let teamgbY = NSLayoutConstraint(item: teamgbLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let teampctY = NSLayoutConstraint(item: teampctLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let teamlY = NSLayoutConstraint(item: teamlLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        let teamwY = NSLayoutConstraint(item: teamwLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        
        NSLayoutConstraint.activate([teamgbRight, teampctRight, teamlRight, teamwRight, teamgbY, teampctY, teamlY, teamwY])
        
        
        teamImage.image = UIImage(named: standings["team"]! as! String)
        teamLabel.text = standings["team"] as! String
        teamwLabel.text = String(format: "%@",  standings["wins"] as! CVarArg)
        teamlLabel.text = String(format: "%@",  standings["loses"] as! CVarArg)
        
        var pctString = String(format: "%@",  standings["winPct"] as! CVarArg)
        
        while pctString.count < 5{
            pctString.append("0")
        }
        
        teampctLabel.text = String(pctString.prefix(5))
        var gb = String(format: "%@",  standings["gamesBack"] as! CVarArg)
        
        if gb.count < 3{
            gb.append(".")
            gb.append("0")
        }
        
        teamgbLabel.text = gb
    }

}
