//
//  scoresTableViewController.swift
//  ballers_mockup
//
//  Created by Wharton, Maxwell E on 4/17/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class scoresTableViewController: UITableViewController, MLBDataProtocol{
    
    let baseball = Baseball()
    var data : [Dictionary<String, Any>]? = nil
    var dataReady = false
    var dataSession = MLBData()
    var date : String? = nil
    var nsDate : Date = Date(){
        didSet{
            let form = DateFormatter()
            form.dateFormat = "MM/dd/yy"
            
            dispDate.text = form.string(from: nsDate)
        }
    }
    
    @IBOutlet weak var dispDate: UILabel!
    
    
    func getNumCells() -> Int{
        let games = self.data!
        
        return games.count
    }
    @IBAction func forwardDay(_ sender: Any) {
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: self.nsDate)
        self.nsDate = tomorrow!
        
        let format = DateFormatter()
        format.dateFormat = "yyyyMMdd"
        self.date = format.string(from: tomorrow!)
        
        self.dataReady = false

        dataSession.getScoresData(date: self.date!)

        tableView.reloadData()
    }
    
    @IBAction func backwardsDay(_ sender: Any) {
        let previous = Calendar.current.date(byAdding: .day, value: -1, to: self.nsDate)
        self.nsDate = previous!
        
        let format = DateFormatter()
        format.dateFormat = "yyyyMMdd"
        self.date = format.string(from: previous!)
        
        self.dataReady = false

        dataSession.getScoresData(date: self.date!)

        tableView.reloadData()
    }
   
    @objc func update(gesture : UIGestureRecognizer){
        self.tableView.reloadData()
    }
    
    private func rotateView(targetView: UIView, duration: Double = 2.5) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(M_PI))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
    
    func addBaseball(){
        self.view.addSubview(self.baseball)
        self.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        tableView.tableFooterView = UIView()
        baseball.translatesAutoresizingMaskIntoConstraints = false
        baseball.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        baseball.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        baseball.widthAnchor.constraint(equalToConstant: 100).isActive = true
        baseball.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.rotateView(targetView: self.baseball)
    }

    override func viewDidLoad() {
        

        super.viewDidLoad()
        self.addBaseball()
        navigationController?.navigationBar.topItem?.title = "Scores"
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(forwardDay(_:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(backwardsDay(_:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(update))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        
        self.date = formatter.string(from: date)
        
        self.dataSession.delegate = self
        
        self.dataSession.getScoresData(date: self.date!)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if !self.dataReady{
            self.baseball.isHidden = false
            return 0
        }
        self.baseball.isHidden = true
        return self.getNumCells()
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath) as! scoresTableCell
        
        let gameData = data![indexPath.row]
        
        cell.awayLogo?.image = UIImage(named: gameData["awayTeam"] as! String)
        cell.homeLogo?.image = UIImage(named: gameData["homeTeam"] as! String)
        cell.awayName.text = gameData["awayTeam"] as? String
        cell.homeName.text = gameData["homeTeam"] as? String
        
        cell.awayLogo.layer.cornerRadius = 20
        cell.homeLogo.layer.cornerRadius = 20
        cell.awayLogo.clipsToBounds = true
        cell.homeLogo.clipsToBounds = true
        
        
        if data![indexPath.row]["status"] as! String == "UNPLAYED" {
            
            cell.timeLabel.isHidden = false
            cell.firstPitchLabel.isHidden = false
            cell.inning.isHidden = true
            cell.errorsLabel.isHidden = true
            cell.hitsLabel.isHidden = true
            cell.runsLabel.isHidden = true
            cell.awayRuns.isHidden = true
            cell.awayHits.isHidden = true
            cell.awayErrors.isHidden = true
            cell.homeHits.isHidden = true
            cell.homeErrors.isHidden = true
            cell.homeRuns.isHidden = true
            
            let timeFormat = DateFormatter()
            timeFormat.dateFormat = "yyyy-MM-dd'T'HH:mm"
            let newFormat = DateFormatter()
            newFormat.dateFormat = "h:mm a"
            
            var startTime = gameData["startTime"] as! String
            
            startTime = String(startTime.prefix(16))
            
            if let date = timeFormat.date(from: startTime){
                startTime = newFormat.string(from: date)
            }
            
            
            cell.timeLabel.text = startTime
            
        }else if data![indexPath.row]["status"] as! String == "LIVE"{
            
            cell.timeLabel.isHidden = true
            cell.firstPitchLabel.isHidden = true

            cell.inning.isHidden = false
            cell.errorsLabel.isHidden = false
            cell.hitsLabel.isHidden = false
            cell.runsLabel.isHidden = false
            cell.awayRuns.isHidden = false
            cell.awayHits.isHidden = false
            cell.awayErrors.isHidden = false
            cell.homeHits.isHidden = false
            cell.homeErrors.isHidden = false
            cell.homeRuns.isHidden = false
            
            cell.inning.text = "Inning " + String(format: "%@", gameData["currentInning"] as! CVarArg)
            cell.homeRuns.text = String(format: "%@", gameData["homeScore"] as! CVarArg)
            cell.homeErrors.text = String(format: "%@", gameData["homeErrors"] as! CVarArg)
            cell.homeHits.text = String(format: "%@", gameData["homeHits"] as! CVarArg)
            cell.awayRuns.text = String(format: "%@", gameData["awayScore"] as! CVarArg)
            cell.awayHits.text = String(format: "%@", gameData["awayHits"] as! CVarArg)
            cell.awayErrors.text = String(format: "%@", gameData["awayErrors"] as! CVarArg)
            

        }else{
            cell.timeLabel.isHidden = true
            cell.firstPitchLabel.isHidden = true
            
            cell.inning.isHidden = false
            cell.errorsLabel.isHidden = false
            cell.hitsLabel.isHidden = false
            cell.runsLabel.isHidden = false
            cell.awayRuns.isHidden = false
            cell.awayHits.isHidden = false
            cell.awayErrors.isHidden = false
            cell.homeHits.isHidden = false
            cell.homeErrors.isHidden = false
            cell.homeRuns.isHidden = false
            
            cell.inning.text = "Finished"
            cell.homeRuns.text = String(format: "%@", gameData["homeScore"] as! CVarArg)
            cell.homeErrors.text = String(format: "%@", gameData["homeErrors"] as! CVarArg)
            cell.homeHits.text = String(format: "%@", gameData["homeHits"] as! CVarArg)
            cell.awayRuns.text = String(format: "%@", gameData["awayScore"] as! CVarArg)
            cell.awayHits.text = String(format: "%@", gameData["awayHits"] as! CVarArg)
            cell.awayErrors.text = String(format: "%@", gameData["awayErrors"] as! CVarArg)
            
        }
        
        cell.layer.borderColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
        cell.layer.borderWidth = 1
        return cell

    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func responseDataHandler(data: Dictionary<String, Any>) {
        self.data = data["data"] as! [Dictionary<String, Any>]

        self.dataReady = true
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func responseError() {
        print("an error has occured")
    }
}
