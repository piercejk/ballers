//
//  Feed.m
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/24/19.
//  Copyright © 2019 ballers. All rights reserved.
//

#import <Foundation/Foundation.h>


NSURL* URL = [NSURL URLWithString: @"https://api.mysportsfeeds.com/v2.1/pull/nhl/{season}/date/{date}/games.{format}}"];
NSDictionary* URLParams = @{
                            @"fordate": @"20161121",
                            };
URL = NSURLByAppendingQueryParameters(URL, URLParams);
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
request.HTTPMethod = @"GET";

// Headers

[request addValue:@"Basic " + base64_encode({apikey_token} + ":" + MYSPORTSFEEDS) forHTTPHeaderField:@"Authorization"];

// Connection

NSURLConnection* connection = [NSURLConnection connectionWithRequest:request delegate:nil];
[connection start];

/*
 * Utils: Add this section before your class implementation
 */

/**
 This creates a new query parameters string from the given NSDictionary.
 */
static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters)
{
    NSMutableArray* parts = [NSMutableArray array];
    [queryParameters enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSString *part = [NSString stringWithFormat: @"%@=%@",
                          [key stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding],
                          [value stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]
                          ];
        [parts addObject:part];
    }];
    return [parts componentsJoinedByString: @"&"];
}

/**
 Creates a new URL by adding the given query parameters.
 @param URL The input URL.
 @param queryParameters The query parameter dictionary to add.
 @return A new NSURL.
 */
static NSURL* NSURLByAppendingQueryParameters(NSURL* URL, NSDictionary* queryParameters)
{
    NSString* URLString = [NSString stringWithFormat:@"%@?%@",
                           [URL absoluteString],
                           NSStringFromQueryParameters(queryParameters),
                           ];
    return [NSURL URLWithString:URLString];
}
