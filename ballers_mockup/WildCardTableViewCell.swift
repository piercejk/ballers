//
//  WildCardTableViewCell.swift
//  ballers_mockup
//
//  Created by Pierce Kotarski on 4/22/19.
//  Copyright © 2019 ballers. All rights reserved.
//

import UIKit

class WildCardTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addStandings(standings : [Dictionary<String, AnyObject>], division : String){
        
        let header = StandingsHeaderView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 30.5))
        header.setupView(division: division)
        
        self.addSubview(header)
        
        let headerLeft = NSLayoutConstraint(item: header, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
        
        let headerTop = NSLayoutConstraint(item: header, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([headerLeft, headerTop])
        
        var previousView = header as UIView
        for eachTeam in standings{
            
            var newView = StandingsRowView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 30.5))
            newView.setupView(standings: eachTeam)
            
            self.addSubview(newView)
            
            var newViewLeft = NSLayoutConstraint(item: newView, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0)
            var newViewTop = NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal, toItem: previousView, attribute: .bottom, multiplier: 1, constant: 0)
            
            NSLayoutConstraint.activate([newViewLeft, newViewTop])
            
            previousView = newView
        }
        
    }

}
